"""
This file makes the correspondence between lgtb varieties
"""

lgtb = {
    "bisexual": "bisexual",
    "trans": "trans",
    "pan": "pan",
    "lesbian": "lesbian",
    "ace": "ace",
    "nb": "nb",
    "gay": "gay",
}

lgtb_synonyms = {
    "bi": lgtb.get("bisexual"),
    "asexual": lgtb.get("ace"),
    "pansexual": lgtb.get("pan"),
    # "non binary": lgtb.get("nb"), # implementation is with only one word
    "non-binary": lgtb.get("nb"),
    "transgender": lgtb.get("trans"),
    "transexual": lgtb.get("trans"),
    "sapphic": lgtb.get("lesbian"),
    "queer": lgtb.get("gay"),
    "lesbiana": lgtb.get("lesbian"),
    **lgtb,
}


def get_lgtb_varieties() -> list:
    return list(set(lgtb_synonyms.values()))
