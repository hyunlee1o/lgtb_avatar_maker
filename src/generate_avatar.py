#!/usr/bin/env python3
import os
from pathlib import Path
import re
import sys
import json
import logging
import io
from src.lgtb import lgtb_synonyms
from src.utils import check_url_online_or_exit
from src.twitter_utils import (
    get_api,
    publish_avatar,
    fetch_masked_avatar,
)
from twitter.error import TwitterError
from twitter.models import Status

logger = logging.getLogger("lgtb_avatar_maker logger")
ROOT_DIR = Path(".").parent
LAST_FILE_SERVED_JSON = ROOT_DIR / "last_served_avatar.json"

ME = "@lgtbavatarmaker"  # name of twitter account
api = get_api()

LATEST_TWEET_ID = api.GetUserTimeline()[0].id
HEROKU = int(os.environ["HEROKU"])

def update_json_file(id_last_tweet_served):
    """
    Adds the latest served id to a file
    """
    with open(LAST_FILE_SERVED_JSON, "w", encoding="utf-8") as json_fp:
        json.dump(
            {"id": id_last_tweet_served}, json_fp, ensure_ascii=False, indent=4
        )
        logger.info(f"Wrote correctly last update code={id_last_tweet_served}")


def process_query(status: Status) -> (str, bytes):
    """
    Builds the tweet and fetches the requested mask

    status:
    status -- a twit asking for an avatar
    """
    username = status.user.screen_name
    text = status.text
    wout_self = text.replace(f"{ME} ", "")
    wout_mentions = re.sub(r"@\w+", "", wout_self)
    tweet = wout_mentions.replace("non binary", "nb")  # special case
    # if a letter of LGTB is not written like the filesystem it gets replaced
    split_tweet = [
        lgtb_synonyms.get(word.lower(), word.lower()) for word in tweet.split()
    ]
    status = f"""
    Here is your avatar @{username}, i hope you like it :)
    This bot allows a user to easily add a ribbon to their avatar without
    giving its personal data to third party apps. #Pride #LGTB #privacy
    """
    if not (
        masked_avatar_bytes := fetch_masked_avatar(api, username, split_tweet)
    ):
        logger.info(f"Couldn't serve avatar image matching {text=}")
        return None, None

    return status, masked_avatar_bytes


def create_requested_masked_avatars():
    """Main function for running the bot online"""
    served_avatars, query_results = 0, None
    # yesterday = str(datetime.date.today() - datetime.timedelta(days=1)).split()[0]

    # Set default
    last_id = LATEST_TWEET_ID

    # update local json
    if not HEROKU:
        try:
            with open(LAST_FILE_SERVED_JSON, "w", encoding="utf-8") as json_fp:

                    # try to get latest id
                    last_tweet_served = json.load(json_fp)

                    # check if json is good format
                    last_id = last_tweet_served.get("id", LATEST_TWEET_ID)

        except (FileNotFoundError, json.JSONDecodeError, io.UnsupportedOperation):

                # skip part if in heroku
                update_json_file(last_id)

    logger.info(f"Before execution: {last_id=}")

    # get tweets
    try:
        query_results = api.GetMentions(since_id=last_id)
    except TwitterError as err:
        logger.warning(
            f"Couldn't get tweets. Probably twitter is misbehaving. Error: {err}"
        )
        sys.exit(0)

    logger.info(f"{len(query_results)=} mentions found")

    # loop through tweets to query the results
    for lgtb_query in query_results:

        # process the tweets and get the avatar
        status, masked_avatar_bytes = process_query(lgtb_query)
        if status is None or masked_avatar_bytes is None:
            continue

        # publish the avatar
        if not (
            response := publish_avatar(
                api, status, masked_avatar_bytes, lgtb_query.id
            )
        ):
            logger.info(f"Couldn't respond to query {lgtb_query.text[:20]}")
            continue

        # get the id of the published tweet
        id_tweet_served = max(last_id, response.id)

        logger.info(
            f"Served an avatar correcty to {lgtb_query.user.screen_name=}."
            f"tweet_url=https://twitter.com/twitter/status/{response.id}"
        )

        # update status of the script
        served_avatars += 1
        last_id = id_tweet_served

    # Dont update if online
    if not HEROKU:
        update_json_file(last_id)

    logger.info(
        f"After execution {served_avatars=} {last_id=}"
    )


def main():
    """
    Execute the main
    """
    check_url_online_or_exit("https://twitter.com")
    create_requested_masked_avatars()


if __name__ == "__main__":
    main()
