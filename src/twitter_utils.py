import tempfile
import glob
import io
import difflib
import logging
import os
import sys
from pathlib import Path
import requests
import twitter
from twitter.error import TwitterError
from PIL import Image
from dotenv import load_dotenv
from src.lgtb import get_lgtb_varieties
from src.image_processing import image_to_byte_array, mask_image

logger = logging.getLogger("lgtb_avatar_maker logger")
ROOT_DIR = Path(".").parent
AVATAR_DIR = ROOT_DIR / "avatars"


def benchmark_filenames_w_query(tags: list, query: list) -> float:
    """Calculates the similarity of a query with a certain filename
    Arguments:
    tags - List of words describing a mask.
           They are embedded in filename like:
               nb_pastel_big_red.png
    query - Splitted list of the inputted words by the user
               nb red
    Example:
    >>>difflib.SequenceMatcher(None, ["nb","pastel","big","red"], ["nb","red"]).ratio()
    0.6666666666666666
    """
    return difflib.SequenceMatcher(None, tags, query).ratio()


def select_mask_from_tweet(split_tweet: list):
    """
    Iterates through the avatars folder and benchmarks
    the best image matching the tweet.
    Returns:
        str - path of the best mask
    """

    if not split_tweet[0] in get_lgtb_varieties():
        logger.info(f"Incorrect call LGTB avatar is not on list=> {split_tweet}")
        return None

    if not (avatars := glob.glob(f"{AVATAR_DIR}/*.png")):
        logger.exception("There are no avatars in the directory")
        sys.exit(1)
        return None

    best_filename, highest_ratio = None, 0
    for avatar_path in avatars:

        avatar_similarity_ratio = benchmark_filenames_w_query(
            os.path.basename(avatar_path).removesuffix(".png").split("_"),
            split_tweet
        )

        if avatar_similarity_ratio > highest_ratio:
            best_filename, highest_ratio = avatar_path, avatar_similarity_ratio

    return best_filename


def get_api():
    """Loads the twitter api"""
    load_dotenv()
    try:
        return twitter.Api(
            consumer_key=os.environ["TWITTER_API_KEY"],
            consumer_secret=os.environ["TWITTER_API_SECRET_KEY"],
            access_token_key=os.environ["TWITTER_ACCESS_TOKEN"],
            access_token_secret=os.environ["TWITTER_ACCESS_TOKEN_SECRET"],
            sleep_on_rate_limit=True,
        )
    except KeyError:
        logger.exception("Missing the twitter api keys")
        sys.exit(1)


def publish_avatar(api, status, image,
                   in_reply_to_status_id) -> twitter.Status:
    """Publish a twitter tweet with an avatar"""
    with tempfile.NamedTemporaryFile(suffix=".png") as fp:
        fp.write(image)
        try:
            return api.PostUpdate(
                status=status,
                media=fp.name,
                in_reply_to_status_id=in_reply_to_status_id,
            )
        except TwitterError:
            logger.warning(
                f"Could not post avatar with {in_reply_to_status_id=}.",
            )
            raise


def fetch_masked_avatar(
        api: twitter.api.Api, username: str, split_tweet: list
) -> Image:
    """
    Selects the adequate mask from the tweet and masks it.

    Returns:
        image in bytes format to be delivered to the api

    """
    if not 0 < len(split_tweet) <= 4:
        logger.debug(f"Incorrect call from @{username}=> {split_tweet}")
        return

    if not (mask_path := select_mask_from_tweet(split_tweet)):
        logger.debug(f"Requested file not found. Query => {split_tweet}")
        return

    masked_avatar_pil = mask_user_avatar(api, username, mask_path)
    masked_avatar_bytes = image_to_byte_array(masked_avatar_pil)
    return masked_avatar_bytes


def mask_user_avatar(api, user, lgtb_choice):
    """
    user is the screen_name eg: @lgtbavatarmaker
    lgtb_choice is a list of words naming the lgtb avatars
    """
    user = api.GetUser(screen_name=user)

    # get profile picture from user
    profile_picture_url = user.profile_image_url.replace("normal", "400x400")

    # download profile picture
    profile_picture = requests.get(profile_picture_url)

    # mask the avatar
    with (Image.open(io.BytesIO(profile_picture.content)) as avatar,
          Image.open(lgtb_choice) as mask):
        mask_image(avatar, mask)
        return avatar
