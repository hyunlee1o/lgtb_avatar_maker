"""
This module contains the image processing functions used in lgtb avatar maker
"""
import io
import logging
from PIL import Image

logger = logging.getLogger("lgtb_avatar_maker logger")


def mask_image(image: Image, mask: Image) -> None:
    """
    This function masks a png image.
    Makes it smaller in case they don't match
    Arguments:
        image - PIL image
        mask - mask

    """
    if image.size < mask.size:
        mask.thumbnail(image.size, Image.ANTIALIAS)
    elif image.size > mask.size:
        image.thumbnail(mask.size, Image.ANTIALIAS)
    image.paste(mask, mask=mask.split()[3])  # png to jpg


def image_to_byte_array(image: Image) -> bytes:
    """
    Converts a PIL image to bytes
    Returns:
        bytes image
    """
    image_bytes = io.BytesIO()
    image.save(image_bytes, format="PNG")
    return image_bytes.getvalue()
