import requests
import logging
import sys

logger = logging.getLogger("lgtb_avatar_maker logger")

def check_url_online_or_exit(url: str) -> None:
    try:
        requests.get(url)
    except requests.exceptions.ConnectionError:
        logger.warning(f"{url=} not reachable. Halting execution")
        sys.exit(1)
