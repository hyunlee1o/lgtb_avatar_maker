"""
Main of the lgtb_avatar_maker
"""
import argparse
from argparse import FileType
import logging
import os.path
import glob
from logging.handlers import RotatingFileHandler
from PIL import Image
from src import generate_avatar
from src.image_processing import mask_image
from pathlib import Path

ROOT_DIR = Path(".").parent
LOG_DIR = ROOT_DIR / "logs"

def get_logger():
    """
    Declares the logger and makes it easy to get it
    """
    try:
        os.mkdir(ROOT_DIR / "logs")
    except FileExistsError:
        pass
    logname = os.path.join(LOG_DIR, "lgtb_avatar_maker.log")
    logger_format = "[%(levelname) 5s/%(asctime)s] %(name)s: %(message)s"
    logging.basicConfig(
        handlers=[
            RotatingFileHandler(logname, maxBytes=100_000, backupCount=5)
        ],
        level=logging.INFO,
        format=logger_format,
        datefmt="%Y-%m-%dT%H:%M:%S",
    )
    logger = logging.getLogger("lgtb_avatar_maker logger")
    return logger

logger = get_logger()
parser = argparse.ArgumentParser(description="LGTB Avatar generator ")
parser.add_argument("mode", type=str,
                    help="""
                    The mode to run the program.
                    You can run it in online mode to setup the bot
                    or in local mode to make yourself an avatar
                    Options: local, online, test
                    """)
parser.add_argument("--mask", type=Path, help="Path of the mask to use")
parser.add_argument("--avatar", type=Path, help="Path of the image to be masked")
parser.add_argument("--output", type=Path, help="Path in which to save the masked image")
args = parser.parse_args()

if args.mode == "online":
    logger.info("Starting lgtb_avatar_maker online bot")
    generate_avatar.main()

elif args.mode == "local":

    if not args.mask or not args.avatar:
        print("Avatar or mask file not found")

    if not args.output:
        print("There is not output set")

    with (Image.open(args.avatar) as avatar,
          Image.open(args.mask) as mask):
        mask_image(avatar, mask)
        avatar.save(args.output, "JPEG")

elif args.mode == "test":
    logger.info("Test logger")
    print("Nothing to do")
else:
    print("Mode not found, available modes are local, online, test")
